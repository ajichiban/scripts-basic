<?php  

# Relaciones 1 a 1
#  un usuario tienen un telefono y un telefono pertenece a un usuario.

### Model User ###
public function phone()
{
    return $this->hasOne('App\Phone');
}
### Model Phone ###
public function user()
{
    return $this->belongsTo('App\User');
}

# Relaciones 1 a Muchos
#  un Post tiene Muchos comentarios , y un comentario pertenece a 1 Post.
### Model Post ###
public function comments()
{
    return $this->hasMany('App\Comment');
}
### Model Comment ###
public function post()
{
    return $this->belongsTo('App\Post');
}


# Relaciones Muchos a Muchos are slightly more complicated than hasOne and hasMany relationships. An example of such a relationship is a user with many roles, where the roles are also shared by other users. For example, many users may have the role of "Admin". To define this relationship, three database tables are needed: users, roles, and role_user. The role_user table is derived from the alphabetical order of the related model names, and contains the user_id and role_id columns.
### Model User ###
public function roles()
{
    return $this->belongsToMany('App\Role');
}
### Model Role ###
public function users()
{
    return $this->belongsToMany('App\User');
}

/**
 * Relaciones  Polimorficas.
 *
 * 
 */

# One to One.
# Structura de las migraciones
/*posts
    id - integer
    name - string

users
    id - integer
    name - string

images
    id - integer
    url - string
    imageable_id - integer
    imageable_type - string*/

# Un  usuario puede tener una imagen , y un post tambien.
### Model Image ###
public function imageable()
{
    return $this->morphTo();
}

### Model Post ###
public function image()
{
    return $this->morphOne('App\Image', 'imageable');
}
### Model User ###
public function image()
{
    return $this->morphOne('App\Image', 'imageable');
}


# One to Many.
# Structura de las migraciones
/*posts
    id - integer
    title - string
    body - text

videos
    id - integer
    title - string
    url - string

comments
    id - integer
    body - text
    commentable_id - integer
    commentable_type - string
*/


### Model Comment ###
public function commentable()
{
    return $this->morphTo();
}
### Model Post ###
public function comments()
{
    return $this->morphMany('App\Comment', 'commentable');
}
### Model Video ###
public function comments()
{
    return $this->morphMany('App\Comment', 'commentable');
}


# Many to Many.
# Structura de las migraciones
/*posts
    id - integer
    name - string

videos
    id - integer
    name - string

tags
    id - integer
    name - string

taggables
    tag_id - integer
    taggable_id - integer
    taggable_type - string
*/

### Model Post ###
public function tags()
{
    return $this->morphToMany('App\Tag', 'taggable');
}

### Model Video ###
public function tags()
{
    return $this->morphToMany('App\Tag', 'taggable');
}

### Model Tag ###
public function posts()
{
    return $this->morphedByMany('App\Post', 'taggable');
}

public function videos()
{
    return $this->morphedByMany('App\Video', 'taggable');
}
