posicionarBuscador();
 
$(window).scroll(function() {    
    posicionarBuscador();
});
 
function posicionarBuscador() {
    var altura_del_header = $('.main-container').outerHeight(true);
    var altura_del_buscador = $('.search-row-wrapper').outerHeight(true);
 
    if ($(window).scrollTop() >= altura_del_header){
        $('.search-row-wrapper').addClass('fixed');
        $('.wrapper').css('margin-top', (altura_del_buscador) + 'px');
    } else {
        $('.search-row-wrapper').removeClass('fixed');
        $('.wrapper').css('margin-top', '0');
    }
}